﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{

    [SerializeField] private Image health;
    [SerializeField] private float delta;
    private float healthvalue;
    private PlayerControl player;
    private float currentHealth;

    void Start()
    {
        player = FindObjectOfType<PlayerControl>();
        healthvalue = player.Health.CurrentHealth / 100.0f;
    }

   
    void Update()
    {
        currentHealth = player.Health.CurrentHealth / 100.0f;
        if (currentHealth > healthvalue)
            healthvalue += delta;
        if (currentHealth < healthvalue)
            healthvalue -= delta;
        if (currentHealth < delta)
            healthvalue = currentHealth;

        health.fillAmount = healthvalue;
    }
}
